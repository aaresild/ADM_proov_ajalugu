const accordions = document.getElementsByClassName("accordion");
     
for (let i = 0; i < accordions.length; i++) {
    accordions[i].onclick = function () {
        const content = this.nextElementSibling;

        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else { 
            content.style.maxHeight = content.scrollHeight + "px";
            
            for (let j = 0; j < accordions.length; j++) {
                const otherContent = accordions[j].nextElementSibling;

                if (i !== j) {
                    otherContent.style.maxHeight = null;
                }                
            }
        }
    }
}

const nextTopic = (topicIndex) => {
    let openNextTopic = accordions[topicIndex].nextElementSibling;
    
    if (topicIndex === accordions.length - 1) {
        openNextTopic.style.maxHeight = null;
        openNextTopic = accordions[0].nextElementSibling;
        openNextTopic.style.maxHeight = openNextTopic.scrollHeight + "px";
    } else {
        openNextTopic.style.maxHeight = null;
        openNextTopic = accordions[topicIndex + 1].nextElementSibling;
        openNextTopic.style.maxHeight = openNextTopic.scrollHeight + "px";
    }
}

const openModalButton = document.getElementById('open-modal-button'),
      modal = document.getElementsByClassName('modal'),
      closeModalButton = document.getElementById('close-modal-button');

openModalButton.onclick = function () {
    modal[0].style.display = 'block';    
}

closeModalButton.onclick = function () {
    modal[0].style.display = 'none';
}